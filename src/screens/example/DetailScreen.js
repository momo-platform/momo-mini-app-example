import { Button, Spacing, Text } from '@momo-platform/component-kits';
import React, { Component } from 'react';
import { View } from 'react-native';
import MiniApi from '@momo-miniapp/api';

export default class DetailScreen extends Component {

    hello = () => {
        MiniApi.showAlert('Title', 'Message', ['Close', 'Ok'], (buttonIndex) => {
            console.warn('ButtonIndex', buttonIndex);
        });
    };


    render() {
        const { params: { message } } = this.props;
        return (
            <View style={{ padding: Spacing.M, backgroundColor: 'white', flex: 1 }}>
                <Text.H4>Hello Detail Screen</Text.H4>
                <Text style={{ marginVertical: Spacing.L }}>{`Your message: ${message}`}</Text>

                <Button type="outline" title="Click me" onPress={this.hello} />
            </View>
        );
    }
}
