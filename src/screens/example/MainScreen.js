import React, { Component } from 'react';
import { View } from 'react-native';
import { Spacing, Text, Input, Button } from '@momo-platform/component-kits';
import DetailScreen from './DetailScreen';
import { connect } from 'react-redux';
import MiniApi from '@momo-miniapp/api';
import { changeMessageAction } from '../../redux/actions/messageAction';
class MainApp extends Component {

    componentDidMount() {
       // 
    }

    goDetail = () => {
        /**
         * use navigator push to open a new screen
         * dont need to define screeen stack
         */
        const { navigator } = this.props;
        navigator.push({
            screen: DetailScreen,
            /**
             * pass your param here
             */
            params: {
                message: this.message,
                products: ['Item 1', 'Item 2'],
            },
            options: {
                title: 'Detail screen'
            }
        });
    };

    onChangeText = (message) => {
        this.message = message;
        const { changeMessage } = this.props;
        changeMessage(message);
    }


    render() {
        const { message } = this.props;
        return (
            <View style={{ flex: 1, padding: Spacing.M, backgroundColor: 'white' }}>
                <Text.H3>Welcome to Momo Mini App</Text.H3>
                <Text.H4 style={{ marginTop: Spacing.L }}>Mini App Main Screen</Text.H4>
                <View style={{ flex: 1, paddingVertical: Spacing.L }}>

                    <Input
                        onChangeText={this.onChangeText}
                        floatingValue="Type your message" />

                    <Button onPress={this.goDetail} title="View Message" />

                    {message.message ? <Text style={{ textAlign: 'center', marginTop: Spacing.L }}>{`Message from Redux: ${message.message}`}</Text> : <View />}
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    message: state.message,
});

const mapDispatchToProps = dispatch => ({
    changeMessage: (data) => dispatch(changeMessageAction(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(MainApp);
