/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Navigation } from '@momo-platform/component-kits';
import appJson from '../app.json';
import MiniAppTest from './screens/example/MainScreen';

export default class MiniAppStack extends React.Component {
    render() {
        const {
            params = {},
            options = { title: appJson.displayName }
        } = this.props;
        return <Navigation
            screen={MiniAppTest}
            params={params}
            options={options} />;
    }
}
