import { MESSAGE_ACTION } from "../actions/messageAction";

const initialState = {
    message: ''
};
const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case MESSAGE_ACTION.CHANGE:
            return {
                ...state,
                message: action.payload
            };
        default:
            return state;
    }
}
export default messageReducer;