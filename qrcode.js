/* eslint-disable max-len */
const qrcode = require('qrcode-terminal');
const appConfig = require('./app.json');

const argv = (() => {
    const argument = {};
    process.argv.slice(2).forEach((element) => {
        const matches = element.match('--([a-zA-Z0-9]+)=(.*)');
        if (matches) {
            argument[matches[1]] = matches[2]
                .replace(/^['"]/, '').replace(/['"]$/, '');
        }
    });
    return argument;
})();

const port = argv.port || 8181;
const host = argv.host || 'localhost';

const args = {
    config: {
        debugHost: `http://${host}:${port}`,
        development: true,
    },
    appId: appConfig.appId
};
qrcode.generate(`momo://?refId=dev_tool&args=[${JSON.stringify(args)}]`, { small: true });