#!/bin/bash

# Check if port argument is provided
if [ -z "$1" ]; then
  echo "Error: Port number is required."
  echo "Usage: $0 <port>"
  exit 1
fi

# Get the Node.js version and set it to a variable
NODE_VERSION=$(node -v)

# Remove the 'v' prefix from the version string
NODE_VERSION=${NODE_VERSION#v}

# Define the version to compare against
TARGET_VERSION="16"

# Compare the versions
if [[ "$NODE_VERSION" > "$TARGET_VERSION" ]]; then
  # echo "Node.js version is greater than 16.x.x. Using --openssl-legacy-provider."
  export NODE_OPTIONS="--openssl-legacy-provider"
else
  # echo "Node.js version is 16.x.x or lower. No need for --openssl-legacy-provider."
fi

# Start the React Native server with the provided port and reset cache
react-native start --host=$(ipconfig getifaddr en0) --port=$1 --reset-cache